
void JLAB_E99118_NucDB()
{
  NucDBManager * dbman = NucDBManager::GetManager(1);

  auto experiment = dbman->GetExperiment("JLAB_E99118");
  if( !experiment ){
    experiment = new NucDBExperiment("JLAB_E99118","JLab E99118");
  }

  // ----------------------------------------------------------------
  // F2p from Rosenbluth separation
  auto ref = new NucDBReference("E99118_Ref","JLab DIS Data Archive",
                       "https://hallcweb.jlab.org/disdata/index.html\n\
                       The data on R=σL/σT were published in Phys. Rev. Lett. 98 142301 (2007).\n\
                       The data on the structure function F2 have been submitted to \n\
                       Phys. Rev. C. A preprint is available from the archive: 1002.1669. \n\
                       http://arxiv.org/abs/1002.1669",
                       NucDBReference::RefType::kWebsite);
  auto meas = experiment->GetMeasurement("F2p");
  if( !meas ) {
    meas = new NucDBMeasurement("F2p","F_{2}^{p}");
    experiment->AddMeasurement(meas);
  }
  meas->ClearDataPoints();
  meas->SetType( NucDB::Type::StructureFunction );
  meas->SetProcesses( {NucDB::Process::DIS, NucDB::Process::Inclusive, NucDB::Process::Rosenbluth} );
  meas->AddRef(ref);
  // F2d from Rosenbluth separation
  auto F2d_meas = experiment->GetMeasurement("F2d");
  if( !F2d_meas ) {
    F2d_meas = new NucDBMeasurement("F2d","F_{2}^{d}");
    experiment->AddMeasurement(F2d_meas);
  }
  F2d_meas->ClearDataPoints();
  F2d_meas->SetType( NucDB::Type::StructureFunction );
  F2d_meas->SetProcesses( {NucDB::Process::DIS, NucDB::Process::Inclusive, NucDB::Process::Rosenbluth} );
  F2d_meas->AddRef(ref);

  auto afile = std::ifstream("experiments/JLAB-E99118/f2_rosenbluth.text");
  //x      Q2    F2-P   D_norcP D_fullP  F2-D   D_norcD D_fullD
  std::string line;

  // Skip the first 6 lines
  for(int i=0; i<7; i++) {
      std::getline(afile, line);
      std::cout << line << std::endl;
  }
  NucDBBinnedVariable  x_var(  "x","x");
  NucDBBinnedVariable  Q2_var( "Qsquared","Q^{2}");
  NucDBDataPoint  * apoint = 0 ;
  NucDBDataPoint  point = NucDBDataPoint();
  point.AddBinVariable(  &x_var);
  point.AddBinVariable( &Q2_var);

  double xbj, Qsq;
  double F2p_val, eF2p_noRC, eF2p_full; 
  double F2d_val, eF2d_noRC, eF2d_full; 

  for( ; std::getline(afile, line); ) {
    if( afile.eof() ) {break;}
    auto iss = std::stringstream(line);
    std::cout << line << std::endl;
    iss >> xbj;
    iss >> Qsq;
    iss >>  F2p_val;
    iss >> eF2p_noRC;
    iss >> eF2p_full;
    iss >>  F2d_val;
    iss >> eF2d_noRC;
    iss >> eF2d_full;

    x_var.SetValueSize(   xbj, 0.001);
    Q2_var.SetValueSize(  Qsq, 0.001);

    //F2p
    point.SetValue( F2p_val  ); 
    point.SetStatError( eF2p_full  ); 
    //point.SetSystError( NucDBErrorBar(eSys1[i],eSys2[i]) ); 
    point.CalculateTotalError();
    meas->AddDataPoint(new NucDBDataPoint(point));

    //F2d
    point.SetValue( F2d_val  ); 
    point.SetStatError( eF2d_full  ); 
    //point.SetSystError( NucDBErrorBar(eSys1[i],eSys2[i]) ); 
    point.CalculateTotalError();
    F2d_meas->AddDataPoint(new NucDBDataPoint(point));
    
  }

  // ----------------------------------------------------------------
  // Print summary and save database.
  experiment->Print();
  dbman->SaveExperiment(experiment);
}
