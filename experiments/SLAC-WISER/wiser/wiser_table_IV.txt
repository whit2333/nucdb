pi- integral invariant cross section
K0(GeV)  P*_max(GeV)  Plab(GeV)  PT(GeV)    PL*(GeV)  Kmin(GeV)  sig_H(ub/GeV^2)  err      sig_d(ub/GeV^2)  err
5.0      1.417        1.0        0.5        0.029     1.36       2.27e1           0.05e1   
5.0      1.417        1.0        0.625      -0.129    1.52       7.82             0.16     2.06e1           0.03e1
5.0      1.417        1.0        0.750      -0.350    1.83       1.88             0.03     6.57             0.06
5.0      1.417        1.0        0.875      -0.678    2.61       1.60e-1          0.06e-1  9.39e-1          0.15e-1
5.0      1.417        2.0        0.500      0.460     2.31       1.07e1           0.02e1
5.0      1.417        2.0        0.625      0.392     2.41       3.77             0.11
5.0      1.417        2.0        0.750      0.308     2.55       1.31             0.04
5.0      1.417        2.0        0.877      0.203     2.75       4.46e-1          0.19e-1
5.0      1.417        2.0        1.00       0.081     3.02       1.48e-1          0.02e-1  4.29e-1   0.06e-1
5.0      1.417        2.0        1.125      -0.064    3.43       3.28e-2          0.33e-2
5.0      1.417        2.0        1.250      -0.235    4.07       6.21e-3          0.57e-3  2.37e-2   0.11e-2
5.0      1.417        3.0        0.625      0.752     3.39       1.35             0.02
5.0      1.417        3.0        0.750      0.697     3.51       4.43e-1          0.09e-1  9.81e-1   0.15e-1
5.0      1.417        3.0        0.875      0.632     3.66       1.57e-1          0.06e-1
5.0      1.417        3.0        1.000      0.556     3.86       5.89e-2          0.33e-2
5.0      1.417        3.0        1.125      0.468     4.12       1.36e-2          0.05e-2  4.12e-2   0.10e-2
5.0      1.417        3.0        1.250      0.368     4.45       3.13e-3          0.56e-3
5.0      1.417        3.0        1.375      0.255     4.90       2.18e-4          0.38e-4
5.0      1.417        4.0        0.875      0.988     4.63       3.14e-2          0.17e-2
5.0      1.417        4.0        1.000      0.932     4.80       6.92e-3          0.38e-3
7.0      1.714        1.0        0.500     -0.052     1.36       3.14e1           0.06e1
7.0      1.714        1.0        0.625     -0.233     1.52       1.03e1           0.01e1    2.64e1    0.03e1
7.0      1.714        1.0        0.750     -0.485     1.83       2.63             0.09
7.0      1.714        1.0        0.875     -0.861     2.61       2.40e-1          0.08e-1   1.30      0.02
7.0      1.714        2.0        0.500     0.358      2.31       1.66e1           0.02e1
7.0      1.714        2.0        0.625     0.280      2.41       6.04e0           0.11e0
7.0      1.714        2.0        0.750     0.183      2.55       2.32e0           0.05e0
7.0      1.714        2.0        0.875     0.065      2.75       8.47e-1          0.19e-1
7.0      1.714        2.0        1.000     -0.076     3.02       2.77e-1          0.11e-1
7.0      1.714        2.0        1.125     -0.242     3.43       6.92e-2          0.35e-2
7.0      1.714        2.0        1.250     -0.438     4.07       1.40e-2          0.10e-2
7.0      1.714        2.0        1.375     -0.669     5.22       1.50e-3          0.16e-3
7.0      1.714        3.0        0.625     0.606      3.39       3.13e0           0.03e0
7.0      1.714        3.0        0.750     0.544      3.51       1.17e0           0.01e0
7.0      1.714        3.0        0.875     0.469      3.66       4.64e-1          0.06e-1
7.0      1.714        3.0        1.000     0.382      3.86       1.74e-1          0.02e-1
7.0      1.714        3.0        1.125     0.282      4.12       5.30e-2          0.07e-2
7.0      1.714        3.0        1.250     0.167      4.45       1.65e-2          0.08e-2
7.0      1.714        3.0        1.375     0.038      4.90       3.78e-3          0.31e-3
7.0      1.714        3.0        1.500     -0.106     5.53       7.27e-4          0.93e-4
7.0      1.714        3.0        1.625     -0.268     6.46       1.24e-4          0.35e-4
7.0      1.714        4.0        0.792      0.792     4.63       2.36e-1          0.06e-1
7.0      1.714        4.0        1.001      0.728     4.81       8.71e-2          0.30e-2
7.0      1.714        4.0        1.125      0.656     5.02       2.88e-2          0.14e-2
7.0      1.714        4.0        1.250      0.573     5.28       7.41e-3          0.52e-3
7.0      1.714        4.0        1.375      0.481     5.61       2.40e-3          0.18e-3
7.0      1.714        4.0        1.500      0.379     6.03       6.56e-4          0.57e-4
7.0      1.714        4.0        1.625      0.267     6.58       7.95e-5          1.74e-5
7.0      1.714        5.0        1.002      1.034     5.78       3.24e-2          0.06e-2
7.0      1.714        5.0        1.125      0.978     5.97       8.76e-3          0.41e-3
7.0      1.714        5.0        1.250      0.913     6.20       2.31e-3          0.11e-3
7.0      1.714        5.0        1.375      0.841     6.49       6.49e-4          0.26e-4         1.84e-3        0.06e-3
7.0      1.714        5.0        1.500      0.761     6.83       6.71e-5          1.41e-5
7.0      1.714        5.5        1.127      1.128     6.46       4.47e-3          0.23e-3
7.0      1.714        5.5        1.250      1.070     6.68       1.12e-3          0.11e-3
7.0      1.714        5.5        1.376      1.004     6.95       8.14e-5          1.57e-5 
9.0      1.967        1.0        0.500      -0.114    1.36       3.77e1           0.09e1 
9.0      1.967        1.0        0.625      -0.315    1.52       1.18e1           0.02e1          3.19e1         0.05e1
9.0      1.967        1.0        0.750      -0.596    1.83       3.08e0           0.08e0 
9.0      1.967        1.0        0.875      -1.014    2.61       3.12e-1          0.08e-1         1.54e0         0.02e0
9.0      1.967        2.0        0.500      0.285     2.31       2.20e1           0.03e1 
9.0      1.967        2.0        0.625      0.199     2.41       8.25e0           0.12e0 
9.0      1.967        2.0        0.750      0.091     2.55       3.06e0           0.05e0 
9.0      1.967        2.0        0.875      -0.040    2.75       1.15e0           0.01e0 
9.0      1.967        2.0        1.000      -0.197    3.02       3.64e-1          0.14e-1 
9.0      1.967        2.0        1.125      -0.382    3.43       9.87e-2          0.61e-2 
9.0      1.967        2.0        1.250      -0.600    4.07       1.87e-2          0.11e-2 
9.0      1.967        2.0        1.375      -0.856    5.22       2.55e-3          0.21e-3 
9.0      1.967        2.0        1.500      -1.162    7.85       3.41e-4          0.83e-4 
9.0      1.967        3.0        0.625      0.506     3.39       4.92e0           0.10e0 
9.0      1.967        3.0        0.750      0.436     3.51       1.95e0           0.02e0 
9.0      1.967        3.0        0.875      0.353     3.66       7.68e-1          0.09e-1 
9.0      1.967        3.0        1.000      0.256     2.86       2.04e-1          0.04e-1 
9.0      1.967        3.0        1.125      0.145     4.12       9.34e-2          0.12e-2 
9.0      1.967        3.0        1.250      0.018     4.45       3.01e-2          0.06e-2 
9.0      1.967        3.0        1.375      -0.126    4.90       8.38e-3          0.28e-3 
9.0      1.967        3.0        1.500      -0.287    5.53       1.46e-3          0.10e-3 
9.0      1.967        3.0        1.625      -0.467    6.45       2.63e-4          0.46e-4 
9.0      1.967        3.0        1.707      -0.596    7.34       1.26e-4          0.31e-4 
9.0      1.967        4.0        0.875      0.657     4.63       4.77e-1          0.09e-1 
9.0      1.967        4.0        1.000      0.586     4.80       1.91e-1          0.01e-1 
9.0      1.967        4.0        1.125      0.504     5.02       6.80e-2          0.13e-2 
9.0      1.967        4.0        1.250      0.413     5.20       2.16e-2          0.06e-2 
9.0      1.967        4.0        1.375      0.310     5.62       6.97e-3          0.50e-3 
9.0      1.967        4.0        1.500      0.197     6.03       2.09e-3          0.08e-3        6.54e-3      0.17e-3
9.0      1.967        4.0        1.625      0.072     6.58       5.79e-4          0.49e-4
9.0      1.967        4.0        1.750      -0.065    7.30       1.07e-4          0.17e-4
9.0      1.967        4.0        1.875      -0.215    8.28       2.24e-5          0.93e-5
9.0      1.967        5.0        1.003      0.869     5.78       1.07e-1          0.02e-1
9.0      1.967        5.0        1.125      0.806     5.97       3.85e-2          0.11e-2

