Path: /HepData/734/d1-x1-y1
Cross section differential in T for the 1996-1997 data sample
Location: T 1,F 4
Q**2 : 2.0 - 20.0 GeV^2
RE : GAMMA* P --> GAMMA P
SQRT(S) : 30.0 - 120.0 GeV
W : 30.0 - 120.0 GeV
x : ABS(T) IN GEV**2
y : D(SIG)/DT IN NB/GEV**2
xdesc	x	xlow	xhigh	y	dy+	dy-	dy+	dy-	
	0.1	0.1	0.1	29.9	+4.1	-4.1	+7.1	-7.1
	0.3	0.3	0.3	8.0	+1.4	-1.4	+1.4	-1.4
	0.5	0.5	0.5	2.13	+0.6	-0.6	+0.69	-0.69
	0.8	0.8	0.8	0.27	+0.12	-0.12	+0.14	-0.14

Path: /HepData/734/d2-x1-y1
Cross section differential in T for the 1999-2000 data sample
Location: T 1,F 4
Q**2 : 4.0 - 80.0 GeV^2
RE : GAMMA* P --> GAMMA P
SQRT(S) : 30.0 - 140.0 GeV
W : 30.0 - 140.0 GeV
x : ABS(T) IN GEV**2
y : D(SIG)/DT IN NB/GEV**2
xdesc	x	xlow	xhigh	y	dy+	dy-	dy+	dy-	
	0.1	0.1	0.1	13.3	+1.9	-1.9	+3.4	-3.4
	0.3	0.3	0.3	3.99	+0.57	-0.57	+0.69	-0.69
	0.5	0.5	0.5	0.9	+0.25	-0.25	+0.3	-0.3
	0.8	0.8	0.8	0.36	+0.09	-0.09	+0.14	-0.14

Path: /HepData/734/d3-x1-y1
Cross section differential in T for the combined data sample
Location: T 1,F 4
Q**2 : 4.0 - 80.0 GeV^2
RE : GAMMA* P --> GAMMA P
SQRT(S) : 30.0 - 140.0 GeV
W : 30.0 - 140.0 GeV
x : ABS(T) IN GEV**2
y : D(SIG)/DT IN NB/GEV**2
xdesc	x	xlow	xhigh	y	dy+	dy-	dy+	dy-	
	0.1	0.1	0.1	12.0	+1.2	-1.2	+2.9	-2.9
	0.3	0.3	0.3	3.44	+0.38	-0.38	+0.61	-0.61
	0.5	0.5	0.5	0.84	+0.17	-0.17	+0.29	-0.29
	0.8	0.8	0.8	0.21	+0.04	-0.04	+0.09	-0.09

Path: /HepData/734/d4-x1-y1
Cross section as a function of Q**2 using the combined data sample
Location: T 2,F 5_7
ABS(T) IN GEV**2 : < 1
RE : GAMMA* P --> GAMMA P
SQRT(S) : 30.0 - 140.0 GeV
W : 30.0 - 140.0 GeV
x : Q**2 IN GEV**2
y : SIG IN NB
xdesc	x	xlow	xhigh	y	dy+	dy-	dy+	dy-	
	3.0	3.0	3.0	15.7	+2.5	-2.5	+3.4	-3.4
	5.25	5.25	5.25	5.7	+1.1	-1.1	+1.4	-1.4
	8.75	8.75	8.75	3.2	+0.49	-0.49	+0.69	-0.69
	15.5	15.5	15.5	1.2	+0.22	-0.22	+0.32	-0.32
	25.0	25.0	25.0	0.7	+0.19	-0.19	+0.19	-0.19
	55.0	55.0	55.0	0.15	+0.05	-0.05	+0.05	-0.05

Path: /HepData/734/d5-x1-y1
Cross section as a function of Q**2 for Bjorken X=0.0018
Location: T 2
ABS(T) IN GEV**2 : < 1
RE : GAMMA* P --> GAMMA P
SQRT(S) : 30.0 - 140.0 GeV
W : 30.0 - 140.0 GeV
X : 0.0018
x : Q**2 IN GEV**2
y : SIG IN NB
xdesc	x	xlow	xhigh	y	dy+	dy-	
	5.25	5.25	5.25	6.74	+0.93	-0.93
	8.75	8.75	8.75	3.25	+0.51	-0.51
	15.5	15.5	15.5	1.45	+0.3	-0.3

Path: /HepData/734/d6-x1-y1
Cross section as a function of W for the 1996-1997 data sample
Location: T 3,F 6_8
ABS(T) IN GEV**2 : < 1
Q**2 : 2.0 - 20.0 GeV^2
RE : GAMMA* P --> GAMMA P
SQRT(S) : 45.0 - 110.0 GeV
x : W IN GEV
y : SIG IN NB
xdesc	x	xlow	xhigh	y	dy+	dy-	dy+	dy-	
	45.0	45.0	45.0	6.5	+0.8	-0.8	+1.1	-1.1
	70.0	70.0	70.0	8.9	+1.3	-1.3	+1.6	-1.6
	90.0	90.0	90.0	11.1	+2.2	-2.2	+2.7	-2.7
	110.0	110.0	110.0	10.1	+4.7	-4.7	+4.6	-4.6

Path: /HepData/734/d7-x1-y1
Cross section as a function of W for the 1999-2000 data sample
Location: T 3,F 6_8
ABS(T) IN GEV**2 : < 1
Q**2 : 4.0 - 80.0 GeV^2
RE : GAMMA* P --> GAMMA P
SQRT(S) : 45.0 - 130.0 GeV
x : W IN GEV
y : SIG IN NB
xdesc	x	xlow	xhigh	y	dy+	dy-	dy+	dy-	
	45.0	45.0	45.0	2.56	+0.36	-0.36	+0.32	-0.32
	70.0	70.0	70.0	2.93	+0.63	-0.63	+0.46	-0.46
	90.0	90.0	90.0	4.45	+0.83	-0.83	+0.82	-0.82
	110.0	110.0	110.0	5.3	+1.4	-1.4	+1.4	-1.4
	130.0	130.0	130.0	6.4	+2.5	-2.5	+2.7	-2.7

Path: /HepData/734/d8-x1-y1
Cross section as a function of W for the combined data sample
Location: T 3,F 6_8
ABS(T) IN GEV**2 : < 1
Q**2 : 4.0 - 80.0 GeV^2
RE : GAMMA* P --> GAMMA P
SQRT(S) : 45.0 - 130.0 GeV
x : W IN GEV
y : SIG IN NB
xdesc	x	xlow	xhigh	y	dy+	dy-	dy+	dy-	
	45.0	45.0	45.0	2.28	+0.21	-0.21	+0.34	-0.34
	70.0	70.0	70.0	2.91	+0.35	-0.35	+0.51	-0.51
	90.0	90.0	90.0	3.97	+0.54	-0.54	+0.85	-0.85
	110.0	110.0	110.0	4.4	+1.0	-1.0	+1.5	-1.5
	130.0	130.0	130.0	6.4	+2.5	-2.5	+2.7	-2.7

