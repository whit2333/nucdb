Jlab E00-110

Results from
http://inspirehep.net/record/1362489

TABLE VII. Unpolarized cross sections in pb·GeV −4 with their statistical and 
asymmetric point-to-point systematic uncertainties for the Kin2 setting, for 
each bin in φ (vertical) and −t (horizontal).

TABLE VIII. Unpolarized cross sections in pb·GeV −4 with their statistical and 
asymmetric point-to-point systematic uncertainties for the Kin3 setting, for 
each bin in φ (vertical) and −t (horizontal).

TABLE IX. Cross-section differences for opposite beam helicities in pb·GeV −4 
with their statistical and asymmetric point-to-point systematic uncertainties 
for the Kin1 setting, for each bin in φ (vertical) and −t
(horizontal).

TABLE X. Cross-section differences for opposite beam helicities in pb·GeV −4 
with their statistical and asymmetric point-to-point systematic uncertainties 
for the Kin2 setting, for each bin in φ (vertical) and −t
(horizontal).

TABLE XI. Cross-section differences for opposite beam helicities in pb·GeV −4 
with their statistical and asymmetric point-to-point systematic uncertainties 
for the Kin3 setting, for each bin in φ (vertical) and −t
(horizontal).

TABLE XII. Unpolarized cross sections in pb·GeV −4 with their statistical and 
asymmetric point-to-point systematic uncertainties for the KinX2 setting, for 
each bin in φ (vertical) and −t (horizontal).

TABLE XIII. Unpolarized cross sections in pb·GeV −4 with their statistical and 
asymmetric point-to-point systematic uncertainties for the KinX3 setting, for 
each bin in φ (vertical) and −t (horizontal).

TABLE XIV. Cross-section differences for opposite beam helicities in pb·GeV −4 
with their statistical and asymmetric point-to-point systematic uncertainties 
for the KinX2 setting, for each bin in φ (vertical) and
−t (horizontal).

TABLE XV. Cross-section differences for opposite beam helicities in pb·GeV −4 
with their statistical and asymmetric point-to-point systematic uncertainties 
for the KinX3 setting, for each bin in φ (vertical) and
−t (horizontal).





