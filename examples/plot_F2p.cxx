#include "NucDBFuncs.h"
#include "NucDBUtil.h"

void plot_F2p()
{
  auto measurements = NucDB::GetMeasurements("F2p");
  std::cout << measurements.size() << " F2p measurements" << std::endl;
  for(auto m : measurements) { m->Print("data");}

  auto mg  = NucDB::CreateMultiGraph(measurements,"x");
  auto leg = new TLegend(0.7,0.7,0.9,0.9);
  NucDB::FillLegend(leg, measurements, mg);

  //std::vector<NucDBBinnedVariable*> bins = {
  //  new NucDBBinnedVariable("Qsquared","Q^{2}",1.0,0.5),
  //  new NucDBBinnedVariable("Qsquared","Q^{2}",2.0,0.5)
  //};
  std::vector<NucDBBinnedVariable> bins = {
    {"Qsquared","Q^{2}",1.0,0.5},
    {"Qsquared","Q^{2}",2.0,0.5},
    {"Qsquared","Q^{2}",3.0,0.5},
    {"Qsquared","Q^{2}",4.0,0.5}
  };

  auto res = NucDB::FilterMeasurements(measurements, bins);
  int i_color = 2;
  for(auto mints : res) {
    NucDB::SetColors(mints.second, i_color);
    auto mg2 = NucDB::CreateMultiGraph(mints.second, "x");
    //mg->Add(mg2);
    i_color++;
  }
  //gStyle->SetPalette(kBlueRedYellow);
  gStyle->SetPalette(kVisibleSpectrum);
  mg->Draw("A pmc plc");
  mg->GetXaxis()->SetRangeUser(0,1.0);
  mg->Draw("A pmc plc");
  leg->Draw();
}
