#include "NucDBBinnedVariable.h"
#include <sstream>
#include <iomanip>


NucDBVariable::NucDBVariable(const char*n, const char* t ) : TNamed(n,t)
{
   fValue = 0.0;
}
//_____________________________________________________________________________
NucDBVariable::~NucDBVariable()= default;
//_____________________________________________________________________________
void NucDBVariable::Print(Option_t *option) const {
      std::cout << std::setw(7) << " " << GetName() << " = " << fValue << "\n";
}
//_____________________________________________________________________________



ClassImp(NucDBDiscreteVariable)
//_____________________________________________________________________________
NucDBDiscreteVariable::NucDBDiscreteVariable(const char* name , const char* title) : TNamed(name,title) {
   fValue=0;
}
//_____________________________________________________________________________
NucDBDiscreteVariable::~NucDBDiscreteVariable()= default;
//_____________________________________________________________________________

void NucDBDiscreteVariable::Print(Option_t *option) const {
   std::cout << std::setw(7) << " " << GetName() << " = " << fValue << "\n";
}
//_____________________________________________________________________________



ClassImp(NucDBBinnedVariable)
//_____________________________________________________________________________
NucDBBinnedVariable::NucDBBinnedVariable(
      const char * name, 
      const char * title, 
      Double_t     y, 
      Double_t     dy) :
   TNamed(name,title), 
   fMinimum(y-dy),
   fMaximum(y+dy),
   fMean(y),
   fAverage(y),
   fSortPriority(-1)
{
   SetBinMinimum(fMinimum);
}
//_____________________________________________________________________________

NucDBBinnedVariable::~NucDBBinnedVariable()
= default;
//_____________________________________________________________________________

NucDBBinnedVariable::NucDBBinnedVariable(const NucDBBinnedVariable& v)
{
   SetNameTitle(v.GetName(),v.GetTitle());
   fMinimum  = v.GetMinimum();
   fMaximum  = v.GetMaximum();
   fMean     = v.GetMean();
   fAverage  = v.GetAverage();
   fCenter   = v.GetCenter();
   fSortPriority = v.fSortPriority; 
}
//_____________________________________________________________________________

const NucDBBinnedVariable& NucDBBinnedVariable::operator+=(const NucDBBinnedVariable& v) {
   // a weight for finding where the mean should be.
   double weight = GetBinSize()/v.GetBinSize(); 
   if(fMinimum > v.GetMinimum() ) { fMinimum = v.GetMinimum();
}
   if(fMaximum < v.GetMaximum() ) { fMaximum = v.GetMaximum();
}
   SetBinMaximum(fMaximum);
   SetBinMinimum(fMinimum);
   fMean    = (weight*fMean + v.fMean)/(weight+1);

   return *this;
}
//______________________________________________________________________________

const NucDBBinnedVariable& NucDBBinnedVariable::operator+( const NucDBBinnedVariable& v) const {
   return( NucDBBinnedVariable(*this) += v );
}
//______________________________________________________________________________

const NucDBBinnedVariable& NucDBBinnedVariable::operator-=(const NucDBBinnedVariable& v) {
   // a weight for finding where the mean should be.
   double weight = GetBinSize()/v.GetBinSize(); 

   double delta_average = fAverage - v.GetAverage();
   double delta_mean    = fMean    - v.fMean;
   double delta_min     = fMinimum - v.GetBinMinimum();
   double delta_max     = fMaximum - v.GetBinMaximum();

   SetBinMaximum(delta_min);
   SetBinMinimum(delta_max);
   fMean       = delta_mean;
   fAverage    = delta_average;

   return *this;
}
//______________________________________________________________________________

const NucDBBinnedVariable& NucDBBinnedVariable::operator-( const NucDBBinnedVariable& v) const {
   return( NucDBBinnedVariable(*this) -= v );
}
//______________________________________________________________________________

NucDBBinnedVariable& NucDBBinnedVariable::operator=(const NucDBBinnedVariable& v) {
   if ( this != &v) {  
      SetNameTitle(v.GetName(),v.GetTitle());
      fMinimum  = v.GetMinimum();
      fMaximum  = v.GetMaximum();
      fMean     = v.GetMean();
      fAverage  = v.GetAverage();
      fSortPriority = v.fSortPriority; 
   }
   return *this;    // Return ref for multiple assignment
}
//_____________________________________________________________________________

bool NucDBBinnedVariable::BinsOverlap(const NucDBBinnedVariable &var) const {
   //       std::cout << " Min " <<  var.GetMinimum() 
   //                 << " <" << fMinimum 
   //                 << " < " << var.GetMaximum() << "\n";
   if( fMinimum >= var.GetMinimum()  && fMinimum <= var.GetMaximum()) { return true;
   } else if(fMaximum >= var.GetMinimum()  && fMaximum <= var.GetMaximum()) { return true;
   } else if( var.GetMinimum() >= GetMinimum()  && var.GetMinimum() <= GetMaximum()) { return true;
   } else if( var.GetMaximum() >= GetMinimum()  && var.GetMaximum() <= GetMaximum()) { return true;
   } else { return false;
}
}
//_____________________________________________________________________________

bool NucDBBinnedVariable::Contains(double v) const {
   return ( v >= GetMinimum() ) && ( v < GetMaximum() );
}
bool NucDBBinnedVariable::IsBelow(double v) const {
   return GetMaximum() < v;
}
bool NucDBBinnedVariable::IsAbove(double v) const {
   return GetMinimum() > v;
}
//_____________________________________________________________________________
void NucDBBinnedVariable::SetBinValueSize(Double_t val, Double_t size) {
   fMean = val;
   SetBinMinimum(val-size/2.0);
   SetBinMaximum(val+size/2.0);
}
//______________________________________________________________________________
void NucDBBinnedVariable::SetAverage(      Double_t val) {
   // keep the bin size but shift the center
   // Note the mean stays the same.
   double size = GetBinSize();
   fAverage = val; 
   fCenter = val; 
   fMinimum=val-size/2.0;
   fMaximum=val+size/2.0;
   if(fMean<fMinimum ) {
      Error("SetAverage","Mean less than bin min");
   }
   if(fMean>fMaximum ) {
      Error("SetAverage","Mean greater than bin max");
   }
}
//_____________________________________________________________________________

void      NucDBBinnedVariable::Print(Option_t *option) const
{
   TString opt1         = option;
   opt1.ToLower();
   Bool_t printjson = opt1.Contains("json") ? kTRUE  : kFALSE;
   if(printjson){

   } else {
     std::cout 
       << std::setw(9) << GetName() << " = " << std::setw(9) << fAverage 
       << "  mean: " << std::setw(9) << fMean << ", center: "<< std::setw(9)  << fCenter 
       << " (" << fMinimum << " < " << GetName() << " < " << fMaximum << ")\n" ;
     //<< " sort: " << fSortPriority << "\n";
   }
}
//_____________________________________________________________________________

const char * NucDBBinnedVariable::GetLegendString() const
{
   std::stringstream  ss;
   ss << "|" << GetTitle() << "|=" << fMean
      << " (" << fMinimum << " < " << GetTitle() << " < " << fMaximum << ")" ;
   //<< " sort: " << fSortPriority << "\n";
   std::string* res = new std::string(ss.str());
   return res->c_str();
}
//______________________________________________________________________________

